#!/bin/bash

# This script is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3, or (at your option) any later version.
# 
# This script is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this script; see the file COPYING. If not see <http://www.gnu.org/licenses/>.

trap '>&2 echo "error: $BASH_SOURCE:$LINENO: $BASH_COMMAND"; exit 1' ERR
set -eE -o pipefail

CONFIG="${TIDE_CONFIG:?must be present in environment}"
DATABASE="${TIDE_DATABASE:?must be present in environment}"

function update_config {
local create_rss_table='
DROP TABLE IF EXISTS rss;
CREATE TABLE rss (
    url TEXT UNIQUE NOT NULL,
    item_xpath TEXT NOT NULL,
    name_xpath TEXT NOT NULL,
    data_xpath TEXT NOT NULL,
    table_name TEXT NOT NULL
);
'
    echo "new '$CONFIG' found..."
    {
        echo "$create_rss_table"
        cat "$CONFIG"
    } | sqlite3 "$DATABASE"
    echo "rss table has been updated with new feed list."
}

function quote {
    sed "s/'/''/g" <<< "$1"
}

function store {
local table="$1"
local create_store_table="
CREATE TABLE IF NOT EXISTS ${table} (
    url TEXT UNIQUE NOT NULL,
    name TEXT NOT NULL,
    added INTEGER DEFAULT (unixepoch()) NOT NULL,
    fetched INTEGER DEFAULT FALSE NOT NULL,

    CHECK(fetched IN (FALSE, TRUE))
);

CREATE INDEX IF NOT EXISTS index_${table}_added
ON ${table} (added);

CREATE INDEX IF NOT EXISTS index_${table}_fetched
ON ${table} (fetched);
"
    echo "BEGIN;"
    echo "$create_store_table"

    local url name
    while read -r url
          read -r name
    do
        [[ ! -z "$url" ]]
        [[ ! -z "$name" ]]
        echo "INSERT OR IGNORE INTO ${table} (url, name) VALUES ('$(quote "$url")', '$(quote "$name")');"
    done

    echo "COMMIT;"
}

function sync {
    [[ "$CONFIG" -nt "$DATABASE" ]] && update_config
    sqlite3 -separator ' ' "$DATABASE" \
    'SELECT url, item_xpath, name_xpath, data_xpath, table_name FROM rss;' | {
        while read -r url item_xpath name_xpath data_xpath table_name
        do
            curl -sS "$url" |
            xmlstarlet sel -T -t -m "$item_xpath" \
                                 -v "$data_xpath" -n \
                                 -v "$name_xpath" -n |
            store "$table_name" |
            sqlite3 "$DATABASE"
        done
    }
}

sync
