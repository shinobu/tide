# tide
This is a simple script that keeps track of content in RSS feeds. Unlike a RSS reader, it simply stores metadata in a database.
## Installation
As it is a very small, simple and specialized script, you will be unlikely to find it in repository of your distribution.
### Arch Linux
You can install it using the provided `PKGBUILD`:
```
$ git clone --depth 1 https://codeberg.org/shinobu/tide
$ cd tide
$ makepkg -si
```
systemd units are provided, but by default they're disabled, all you have to do is enable the timer if you want current user to sync with the feeds:
```
$ systemctl --user daemon-reload
$ systemctl --user enable tide.timer
$ systemctl --user start tide.timer
```
Default timer is set to trigger every hour.
### Other
If you're using systemd, you can install `tide` manually to your own user.
Remember to patch `tide.service` to point to where you installed `tide`.
```bash
$ git clone --depth 1 https://codeberg.org/shinobu/tide
$ install -Dm744 "tide/tide.sh" "$HOME/.local/bin/tide"
$ install -Dm644 "tide/tide.service" -t "$HOME/.config/systemd/user/"
$ install -Dm644 "tide/tide.timer" -t "$HOME/.config/systemd/user/"
```
If not, read documentation of your own init system.
## Configuration
`tide` is configured using environment variables, it is recommended to put this in your `~/.bashrc`:
```bash
export TIDE_CONFIG="$HOME/.config/tide/rss.sql"
export TIDE_DATABASE="$HOME/.local/share/tide.sqlite"
```
Note that while using `systemd`, the `tide.service` is configured using `Environment` parameter under `[Install]`, if you don't like the defaults, you can change them using:
```
$ systemctl --user edit tide.service
```
`tide` primarily relies on `sqlite` database and so its feed configuration is part of it. One thing you want it to do is to insert RSS feeds you care about into its own table. You can do this by creating a file pointed to by `$TIDE_CONFIG` which will be loaded next time tide runs and notices your changes.
`rss` table schema:
- `url` - the address of RSS feed;
- `table_name` - name of the table where to store RSS feed data.
- `item_xpath` - `xpath` that selects item in the RSS feed.
- `data_xpath` - `xpath` relative to `item_xpath` that selects value for `table_name.url`
- `name_xpath` - `xpath` relative to `item_xpath` that selects value for `table_name.name`

### Example
```sql
/*
This is SQL file that will be included by tide, you can put *any* valid SQL in here;
`tide` does not need any table besides `rss` and tables that you name yourself in
`table_name`, and so you can do whatever you want with it.
*/
BEGIN;

INSERT INTO rss
(
    url,
    item_xpath,
    name_xpath,
    data_xpath,
    table_name
)
VALUES
(
    'https://nyaa.si/?page=rss',
    'rss/channel/item',
    'title',
    'link',
    'anime'
),
(
    'https://nitter.net/linuxfoundation/rss',
    'rss/channel/item',
    'title',
    'link',
    'nitter_linuxfoundation'
);

COMMIT;
```
This example configuration includes two feeds:
 - anime titles and torrent urls in `anime` table;
 - nitter tweets and urls to them in `nitter_linuxfoundation` table;
 
 You can always inspect the database manually:
```
$ sqlite3 ~/.local/share/tide.sqlite
SQLite version 3.38.4
Enter ".help" for usage hints.
sqlite> .mode table
sqlite> .tables
anime                   nitter_linuxfoundation  rss
sqlite> SELECT table_name, item_xpath, name_xpath, data_xpath FROM rss;
+------------------------+------------------+------------+------------+
|       table_name       |    item_xpath    | name_xpath | data_xpath |
+------------------------+------------------+------------+------------+
| anime                  | rss/channel/item | title      | link       |
| nitter_linuxfoundation | rss/channel/item | title      | link       |
+------------------------+------------------+------------+------------+
sqlite> SELECT url FROM nitter_linuxfoundation WHERE downloaded = FALSE LIMIT 5;
+--------------------------------------------------------------+
|                             url                              |
+--------------------------------------------------------------+
| https://nitter.net/LF_Training/status/1524337775394361345#m  |
+--------------------------------------------------------------+
| https://nitter.net/LF_Training/status/1524252463716605954#m  |
+--------------------------------------------------------------+
| https://nitter.net/LF_Training/status/1524189046058020865#m  |
+--------------------------------------------------------------+
| https://nitter.net/LF_Training/status/1524147019744485381#m  |
+--------------------------------------------------------------+
| https://nitter.net/linuxfoundation/status/152413910058810572 |
| 8#m                                                          |
+--------------------------------------------------------------+
```
## Usage
`tide` has no user interface, it is intended to be left unattended. To sync with rss feeds manually, you just have to execute the script:
```
$ tide
```
## Scripting
Because `tide` only populates a `sqlite` database, you can script any actions you need, thanks to its commandline utility `sqlite3`, which can interpret SQL queries directly from commandline.
### Example
```bash
#!/bin/bash

# it is very convenient to setup our script to print the error and
# exit as soon as possible so that we have less things to worry about
trap '>&2 echo "error: $BASH_SOURCE:$LINENO: $BASH_COMMAND"; exit 1' ERR
set -eE -o pipefail

# if tide was configured properly, we don't need to hardcode database location
DATABASE="${TIDE_DATABASE:?must be present in environment}"

# it is a good idea to make this script reusable
# in this example, user can fetch data from remote using metadata in a specific
# table to a specific directory, making its reusability very high - as soon as
# there's something new of interest added to any table, it can be downloaded
# using only this one script no matter where it is saved nor where it needs to go
table="${1:?expected a name of the table to query content location from}"
directory="${2:?expected a directory to fetch content into}"


# it is important to set a separator to an empty space here
# so that we can directly interpret it as bash arguments
# using `read` command
# 
# this SQL query returns a list of record id's and urls that
# we have not fetched data from, that can be directly `read`
# with no further processing, which is what we want
sqlite3 -separator ' ' "$DATABASE" "
SELECT
    rowid, url
FROM
    ${table} 
WHERE
    fetched = FALSE;" |
{ while read -r id url; do
# here between the braces, stdout is piped to sqlite database;
# because of that, we have to redirect wget to stderr to prevent
# sqlite from reading that which does not concern it
>&2 wget --content-disposition -P "$directory" "$url"
# now that the file is downloaded and the script did not exit
# by this point, we can mark this row as fetched in our database,
# so that on the next run, this record is not brought up anymore
echo "
UPDATE
    ${table}
SET
    fetched = TRUE
WHERE 
    rowid = ${id};"
done } |
sqlite3 "$DATABASE"
```
## License
```
This script is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This script is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>. 
```
